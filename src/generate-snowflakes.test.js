const { _Snowflake, generateSnowflakes } = require("./generate-snowflakes")

const fastCheck = require("@fast-check/jest")

const snowflakeSrc = "snowflake.svg"

describe("_Snowflake", () => {
    test("called with layer 0", () => {
        const snowflake = _Snowflake([snowflakeSrc, 0, 0])
        expect(snowflake.dataset.layer).toEqual("0")
        expect(snowflake.src.endsWith("snowflake.svg")).toBe(true)
        expect(snowflake.style.position).toEqual("absolute")
    })
})

describe("generateSnowflakes", () => {
    const { fc } = fastCheck
    const pickSnowflakeCount = fc.nat({max: 500})
    const pickLayerCount = fc.integer({min: 1, max: 500})
    fastCheck.test.prop([pickSnowflakeCount, pickLayerCount])(
        "Should generate correct number of snowflakes",
        (nFlakes, nLayers) => {
            const actual = generateSnowflakes(nFlakes, nLayers, snowflakeSrc)
            return actual.length === nFlakes
        })
    fastCheck.test.prop([pickSnowflakeCount, pickLayerCount])(
        "0 < snowflake.dataset.layer <= layerCount",
        (nFlakes, nLayers) => {
            const actual = generateSnowflakes(nFlakes, nLayers, snowflakeSrc)
            return actual.every(snowflake => {
                const layer = Number(snowflake.dataset.layer)
                const zi = Number(snowflake.style.zIndex)
                return zi == layer &&  0 < layer && layer <= nLayers
            })
        })
    test("generating 0 snowflakes", () => {
        const snowflakes = generateSnowflakes(0, 0, snowflakeSrc)
        expect(snowflakes.length).toEqual(0)
    })
    test("generating many snowflakes", () => {
        const snowflakes = generateSnowflakes(3, 4, snowflakeSrc)
    })
})
