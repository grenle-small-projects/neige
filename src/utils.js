
/**
 * Create an array of n thing's
 * @template T
 * @param {number} n
 * @param {T} thing
 * @returns {Array<T>}
 */
function filledArray(n, thing){
    return Array(n).fill(thing)
}

/**
 * @template T
 * @param {number} n
 * @param {() => T} proc
 * @returns {Array<T>}
 */
function filledArrayProc(n, proc){
    return [...Array(n)].map(_ => proc())
}

/**
 * Similar to the tabulate functions on lists and arrays
 * found in the SML Basis.
 * @template T
 * @param {number} n
 * @param {(index: number) => T} f
 * @return {Array<T>}
 */
function tabulate(n, f){
    return [...Array(n)].map((_, i) => f(i))
}

/**
 * A random number between 0 and n - 1.
 * If n = 0, the procedure is equivalent to K(0).
 * If n < 0, the procedure is equivalent to - (f(- n)).
 * @param {number} n
 */
function randomNaturalUpTo(n){
    return Math.floor(Math.random() * n)
}

/**
 * @template T
 * @param {Array<(arg: T) => any>} procedures
 */
function sequence(...procedures){
    /** @param {T} arg */
    return function(arg){
        procedures.forEach(f => f(arg))
    }
}

/** @type {(element: HTMLElement) => void}  */
const bodyAppend = element => document.body.append(element)

/** @type {(t: number) => Promise<void>} */
const sleep = t => new Promise(r => setTimeout(r, t))

module.exports = {
    filledArray,
    filledArrayProc,
    tabulate,
    randomNaturalUpTo,
    sequence,
    bodyAppend,
    sleep
}
