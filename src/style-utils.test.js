// @ts-check

const fastCheck = require("@fast-check/jest")

const { randomLeft, pxValueSetter } = require("./style-utils")

describe("pxValueSetter", () => {
    const { fc } = fastCheck
    const test = fastCheck.test.prop([fc.integer({min: 1})])
    test("Assign number as \"<number>px\" string", value => {
        const el = document.createElement("img")
        const prop = "width"
        pxValueSetter(prop)(el, value)
        return el.style.width === `${value}px`
    })
})


describe("randomLeft", () => {
    const { fc } = fastCheck
    fastCheck.test.prop([fc.integer({min: 1})])(
        "Never exceeds width", innerWidth => {
            const window = { innerWidth }
            const randomValue = randomLeft(window)
            return randomValue >= 0 && randomValue <= innerWidth
        })
})
