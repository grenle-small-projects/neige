//@ts-check

const {
    randomNaturalUpTo,
    filledArray,
    tabulate,
    sequence,
    sleep,
    bodyAppend,
} = require("./utils")

const { randomLeft, setLeft, setTop } = require("./style-utils")

const { getSnowflakeSrc } = require("./get-snowflake-src")

const { generateSnowflakes } = require("./generate-snowflakes")

/**
 * Repositions an out of view snowflake to maintain the
 * illusion of continous flow with a bound number of
 * snowflakes.
 * @param {HTMLImageElement} flake
 */
function respawnSnowflake(flake) {
    flake.style.transition = "none"
    setLeft(flake, randomLeft(window))
    setTop(flake, -Number(flake.dataset.size)) // just out of view
}

/**
 * Establish the transition duration for a layer.
 * @param {number} layer
 */
function transitionDuration(layer) {
    const baseSpeed = window.innerHeight * 7
    const layerContribution = layer * 400
    return baseSpeed - layerContribution
}

/**
 * Pick a delay for the snowflake transition. Even if two
 * snowflakes respawn concurrently, they will diverge.
 */
function randomTransitionDelay() {
    return randomNaturalUpTo(window.innerHeight * 4)
}

/**
 * Get the snowflake's layer data attribute as a number.
 * @param {HTMLImageElement} snowflake
 * @returns {number}
 */
function getSnowflakeLayer(snowflake){
    return Number(snowflake.dataset.layer)
}

/** @param {HTMLImageElement} snowflake */
function addSnowflakeTransition(snowflake) {
    const layer = getSnowflakeLayer(snowflake)
    const duration = transitionDuration(layer)
    const delay = randomTransitionDelay()
    snowflake.style.transition = `top ${duration}ms linear ${delay}ms`
}

/** @param {any} thing */
function isPromise(thing){
    return thing && thing.constructor === Promise
}

/**
 * @template T
 * @param {Array<(arg: T) => any>} procs
 */
function sequenceEffects(...procs){
    /**
     * @param {any} arg
     */
    return async function(arg){
        for(const proc of procs){
            const temp = proc(arg)
            if(isPromise(temp)){
                await temp
            }
        }
    }
}


/** @param {HTMLImageElement} snowflake */
function setSnowflakeDestination(snowflake) {
    const winHeight = window.innerHeight
    const flakeHeight = snowflake.offsetWidth
    const destinationTop = winHeight + flakeHeight
    const top = `${destinationTop}px`
    snowflake.style.top = top
}

const spawnAndAnimate = sequenceEffects(
    respawnSnowflake,
    // A delay is required for some reason...
    _snowflake => sleep(0.01),
    addSnowflakeTransition,
    setSnowflakeDestination,
)

/** @param{HTMLImageElement} snowflake */
async function flakeAnimationLoop(snowflake) {
    snowflake.addEventListener("transitionend", () => {
        flakeAnimationLoop(snowflake)
    }, { once: true })  
    await spawnAndAnimate(snowflake)
}


/** @param {Event} _event */
function init(_event) {
    const imageSource = getSnowflakeSrc()
    const appendAndAnimate = sequence(bodyAppend, flakeAnimationLoop)
    if (imageSource) {
        const snowflakes = generateSnowflakes(600, 4, imageSource)
        snowflakes.forEach(appendAndAnimate)
    }
}

globalThis.addEventListener("load", init)
