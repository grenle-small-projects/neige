const {
    filledArray,
    filledArrayProc,
    tabulate,
    randomNaturalUpTo,
    sequence,
    bodyAppend,
    sleep } = require("./utils")


const fastCheck = require("@fast-check/jest")


/** @type {(_arg: any) => void} */
const noop = _arg => {}


describe("sequence", () => {
    test("empty sequences just return", () => {
        sequence()
    })
    test("single procedure is called", () => {
        const f = jest.fn(noop)
        sequence(f)("boobidoo")
        expect(f.mock.calls).toHaveLength(1)
    })
    test("single procedure effect", () => {
        const obj = { x: 9 }
        const f = o => o.x = o.x + 1
        sequence(f)(obj)
        expect(obj.x).toEqual(10)
    })
    test("two procedures are called", () => {
        const [f, g] = [noop, noop].map(jest.fn)
        sequence(f, g)("boobidoo")
        expect(f.mock.calls).toHaveLength(1)
        expect(g.mock.calls).toHaveLength(1)
    })
    test("procedure effects happend in order", () => {
        let obj = { x: 0 }
        const f = o => o.x = o.x + 1
        const g = o => o.x = o.x * 2
        sequence(f, g)(obj)
        expect(obj.x).toBe(2)
    })
})


describe("sleep", () => {
    test("sleep stops, imperative progress resumes", async () => {
        await sleep(100)
        expect(true).toBeTruthy()
    })
    test("sleep has no effect on other commands", async () => {
        const o = { x: 1 }
        await sleep(100)
        o.x = 3
        expect(o.x).toEqual(3)
    })
})


describe("filledArray", () => {
    test("filledArray with length 0", () => {
        const xs = filledArray(0, "A")
        expect(xs).toHaveLength(0)
    })
    test("filledArray with length 1", () => {
        const xs = filledArray(1, "A")
        expect(xs).toEqual(["A"])
    })
    test("filledArray with length 1", () => {
        const xs = filledArray(3, 639)
        expect(xs).toEqual([639, 639, 639])
    })
})


describe("filleArrayProc", () => {
    test("filledArrayProc with length 0", () => {
	const xs = filledArrayProc(0, () => 3)
	expect(xs).toHaveLength(0)
    })
    test("filledArrayProc with length 1 & K \"A\"", () => {
	const xs = filledArrayProc(1, () => "A")
	expect(xs).toEqual(["A"])
    })
})


describe("tabulate", () => {
    test("tabulate with length 0 and I", () => {
        const xs = tabulate(0, index => index)
        expect(xs).toEqual([])
    })
    test("tabulate with I counts from 0 to n - 1", () => {
        const xs = tabulate(3, index => index)
        expect(xs).toEqual([0, 1, 2])
    })
    test("tabulate with squares", () => {
        const xs = tabulate(3, index => index * index)
        expect(xs).toEqual([0, 1, 4])
    })
})


describe("randomNaturalUpTo", () => {
    const { fc } = fastCheck
    fastCheck.test.prop([fc.integer({min: 1})])(
        "Should not exceed limit or go below 0", (n) => {
            const actual = randomNaturalUpTo(n)
            return actual > -1 && actual < n
        })
    // really, I need a constant here
    fastCheck.test.prop([fc.integer({min: 0, max: 0})])(
        "With n = 0, procedure is K 0", n => {
            const actual = randomNaturalUpTo(n)
            return actual === 0
        })
    fastCheck.test.prop([fc.integer({max: -1})])(
        "If n < 0, the procedure is - f(- n)", n => {
            const actual = randomNaturalUpTo(n)
            return actual >= n && actual < 0
        })
})

describe("bodyAppend", () => {
    test("bodyAppend appends the element to body", () => {
        const appendage = document.createElement("div")
        appendage.setAttribute("id", "appendage")
        bodyAppend(appendage)
        expect(document.querySelector("#appendage").id)
            .toEqual("appendage")
        document.body.removeChild(appendage)
    })
})
