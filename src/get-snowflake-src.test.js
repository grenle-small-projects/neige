const { getSnowflakeSrc } = require("./get-snowflake-src")

describe("getSnowflakeSrc", () => {
    test("no #snowflake", () => {
        const snowflakeSrc = getSnowflakeSrc()
        expect(snowflakeSrc).toEqual(null)
    })
    test("#snowflake isn't an image", () => {
        const snowflake = document.createElement("div")
        snowflake.setAttribute("id", "snowflake")
        document.body.append(snowflake)
        const snowflakeSrc = getSnowflakeSrc()
        expect(snowflakeSrc).toEqual(null)
        document.body.removeChild(snowflake)
    })
    test("#snowflake is an image with no or empty src", () => {
        const snowflake = document.createElement("img")
        snowflake.setAttribute("id", "snowflake")
        document.body.append(snowflake)
        const snowflakeSrc = getSnowflakeSrc()
        expect(snowflakeSrc).toEqual(null)
        document.body.removeChild(snowflake)
    })
    test("#snowflake is an image with", () => {
        const snowflake = document.createElement("img")
        snowflake.setAttribute("id", "snowflake")
        snowflake.src = "./snowflake.svg"
        document.body.append(snowflake)
        const snowflakeSrc = getSnowflakeSrc()
        expect(snowflakeSrc.endsWith("/snowflake.svg")).toBe(true)
        document.body.removeChild(snowflake)
    })
})
