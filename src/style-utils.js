// @ts-check

const { randomNaturalUpTo } = require("./utils")

/** @type {(window: Window) => number} */
const randomLeft = (window) => randomNaturalUpTo(window.innerWidth)

/**
 * @type {(prop: string) => (el: HTMLElement, value: number) => string}
 */
const pxValueSetter = prop =>
      (el, value) => el.style[prop] = `${value}px`

const setLeft = pxValueSetter("left")
const setTop = pxValueSetter("top")

module.exports = { randomLeft, pxValueSetter, setLeft, setTop }
