// @ts-check

/**
 * @param {[string, number, number]} arg
 */
function Snowflake([src, layer, layerCount]) {
    const flake = document.createElement("img")
    const size = 6 + layer
    flake.src = src
    // having the size as part of the dataset avoids querying the
    // DOM. This fixed part of the layout thrashing
    flake.dataset.size = `${size}`
    flake.style.opacity = `${layer/layerCount}`
    flake.dataset.layer = `${layer}`
    flake.style.zIndex = `${layer}`
    flake.style.width = `${size}px`
    flake.style.height = `${size}px`
    flake.style.position = "absolute"
    return flake
}

/**
 * Create n flakes
 * @param {number} snowflakeCount
 * @param {number} layerCount
 * @param {string} imageSrc
 */
function generateSnowflakes(
    snowflakeCount, layerCount, imageSrc) {
    return [...new Array(snowflakeCount)]
        .map((_, i) => Snowflake(
            [imageSrc, i % layerCount + 1, layerCount]))
}

module.exports = { _Snowflake: Snowflake, generateSnowflakes }
