/**
 * Get the src for the snowflakes.
 *
 * Getting the src that way side stepsissues with file
 * names being mangled by bundlers and other tools.
 *
 * @returns {string|null}
 */
function getSnowflakeSrc() {
    /** @type {HTMLImageElement | null} */
    const flakeElement = document.querySelector("#snowflake")
    if (flakeElement && flakeElement.nodeName === "IMG") {
        const flakeImageSrc = flakeElement.src
	if(flakeImageSrc.length !== 0){
	    return flakeImageSrc
	}
	return null
    }
    return null
}


module.exports = { getSnowflakeSrc }
