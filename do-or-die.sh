#!/usr/bin/env bash

# stolen with modifications from:
# https://nvoulgaris.com/test--commit--revert/

function test() {
  echo "Testing changes..."
  npm run test
}

function commit() {
  echo "Committing changes"
  git add . && git commit -m "WIP"
}

function revert() {
  echo "Reverting changes"
  git checkout .
}

test && commit || revert
